
import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:peliculas/models/peliculas.dart';

class PeliculasProvider {

  String _apikey = "7e79e8ed7922e89ab2d2a8ebee5b0bf7";
  String _url = "api.themoviedb.org";
  String _laguage = "es-ES";

  Future<List<Pelicula>> _procesarRespuesta(Uri uri) async {
    final resp = await http.get(uri);
    final decodedData = json.decode(resp.body);

    final peliculas = Pelicula.fromJsonList(decodedData['results']);
  }


  Future<List<Pelicula>> getEnCartelera() async{
    final url = Uri.https(this._url, '3/movie/now_playing', {
      'api_key': this._apikey,
      'language': this._laguage
    });

    return null;

  }

}