
import 'package:flutter/material.dart';
import 'package:peliculas/widgets/card_swiper_widget.dart';

class HomePage extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Peliculas'),
        ),
      body: Container(
        child: Column(
          children: <Widget>[
            _footer(context)
          ],
        ),
      )
    );
  }

}

Widget _footer(BuildContext context){
  return Container(
    width: double.infinity,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(left: 20.0),
          child: Text('Los mas populares', style: Theme.of(context).textTheme.subhead),
        )
      ],
    ),
  );
}