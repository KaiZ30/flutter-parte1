import 'package:flutter/material.dart';
import 'package:peliculas/pages/home_page.dart';

void main() => runApp(MiApp());

class MiApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Peliculas',
      initialRoute: 'inicial',
      routes: {
        '/': (BuildContext context) => HomePage(),
      },
    );
  }

}